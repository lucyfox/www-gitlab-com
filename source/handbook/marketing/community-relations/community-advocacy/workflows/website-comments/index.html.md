---
layout: markdown_page
title: "Website comments workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Workflow

- When the queue of the comments is manageable:
  1. Go through the tickets per-post
  1. See if all comments have received a response
  1. If any comment needs a response, open the link from the ticket and respond **from your personal Disqus account** outside of Zendesk
  1. Then mark all the relevant tickets in Zendesk as `Solved`
  
- When there is a backlog and you're trying to respond to every comment as quickly as possible:
  1. Go through the tickets per-post
  1. See if all comments have received a response
  1. If any comment needs a response, reply **directly from Zendesk** using the Tanukidesk integration
  
## Best practices

Monitor the `#docs-comments` and `#mentions-of-gitlab` Slack channels for possible internal discussions

## Automation

All comments from our website are handled by Disqus and we developed a native Zendesk integration for them - [Tanukidesk](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/tanukidesk). It pipes these posts to `website comments` ZenDesk view as tickets.
