---
layout: markdown_page
title: "Biggest risks"
---
## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

People frequently ask the CEO things like:

- What are the biggest risks to the company?
- What are your biggest fears?
- What keeps you up at night?

On this page, we document the biggest risks and how we intend to mitigate them.

## Lowering the hiring bar

We are growing rapidly (more than doubling headcount in CY2019 again), and there is pressure on departments to meet their [hiring targets](/handbook/hiring/charts/).
It is better for us to miss our targets than to hire people who won't be able to perform to our standards since that takes much longer to resolve.
To ensure the people we hire make the company better, we:

1. Have a standard interview structure
1. Review the interview scores of new hires to look for trends
1. [Identify and take action on underperformance](/handbook/underperformance)
1. (make this unneeded) Have the CPO and CEO sample new hires and review manager, [staff engineer](/job-families/engineering/developer/#staff-developer), [principal product manager](/job-families/product/product-manager/index.html.md#principal-product-manager) and up hires
1. Compare the external title with the new title being given
1. Conduct bar raiser interviews
1. Review cohort performance in the company (completion of onboarding tasks, bonuses, performance review, 360 feedback, performance indicators)

## Underperformance

In a similar vein, it is important that we do not slow down, which means being very proactive in addressing [underperformance](/handbook/underperformance/). 
We should [identify and take action as early as possible](/handbook/underperformance/#identify-and-take-action-as-early-as-possible).

## Ineffective onboarding

We are onboarding many people quickly, making it easy for things to fall behind.
Therefore we:

1. Measure the onboarding time
1. Measure the time to productivity in sales (ramp time) and engineering (time to first MR, MRs per engineer per month)
1. Make sure we work [handbook-first](/handbook/handbook-usage/#why-handbook-first), so the handbook is up to date.

## Confusion about the expected output

As we add more layers of management to accommodate the new people, it's easy to become confused about what is expected of you.

To make sure this is clear we:

1. Document who is the [DRI](/handbook/people-operations/directly-responsible-individuals/) on a decision.
1. Have clear [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis) across the company
1. Have [Key monthly reviews](/handbook/finance/operating-metrics/#key-monthly-review)
1. Have a job family that includes performance indicators
1. Have a [clear org-chart](/company/team/org-chart/) where [each individual reports to exactly one person](/handbook/leadership/#no-matrix-organization)

## Loss of the values that bind us

It's easy for a culture to get diluted if a company is growing fast.
To make our [values](/handbook/values/) stronger, we:

1. Regularly add to them and update them
1. Find new ways to [reinforce our values](/handbook/values/#how-do-we-reinforce-our-values)

## Loss of the open source community

1. Keep our [promises](/company/stewardship/#promises)
1. Keep listening
1. Assign [Merge request coaches](/job-families/expert/merge-request-coach/)
1. [Contributing organizations](/community/contributing-orgs/)

## Loss of velocity

Most companies start shipping more slowly as they grow.
To keep our pace, we need to:

1. Ensure we get 10 Merge Requests (MRs) per engineer per month
1. Have [acquired organizations](/handbook/acquisitions/) remake their functionality inside our [single application](/handbook/product/single-application/)
1. Have a [quality group](/handbook/engineering/quality/) that keeps our developer tooling efficient
1. Achieve our [category maturity targets](/handbook/product/metrics/#category-maturity-achievement)
1. Ensure each [group has non-overlapping scope](/handbook/team/structure/#product-groups)

## Fork and commoditize

Since we are based on an open source product, there is the risk of fork and commoditize like what [AWS experienced with ElasticSearch](https://www.youtube.com/watch?v=G6ZupYzr_Zg).

This risk is reduced, because we're application software instead of infrastructure software.
Application software is less likely to be forked and commoditized for the following reasons:

| Type of software | Application software | Infrastructure software | |
| Interface | Graphical User Interface (GUI) | Application Programming Interface (API) | A GUI is harder to commoditize than an API |
| Compute usage | Drives little compute | Drives lots of compute | Hyperclouds want to drive compute |
| Deployment | Multi-tenant (GitLab.com) | Single tenant managed service (MongDB Atlas) | Hyperclouds offer mostly managed services |
| Feature richness | Lots of proprietary features  | Few proprietary features | More proprietary features make it harder to commoditize |
| Ecosystem activity | Lots of contributions | Few contributions | Infrastructure is more complex to contribute to. ???? |

What we need to do is:

1. [Keep up velocity](#loss-of-velocity)
1. [Keep the open source community contributing](#loss-of-the-open-source-community)
1. [Follow our buyer-based-open-core pricing model](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier)

## Competition

### Operational excellence

We will always have competition. To deal with competition, operational excellence can be a [surprisingly durable competitive advantage](https://twitter.com/patrickc/status/1090387536520728576).

We encourage operational excellence in the following ways:

1. [Efficiency value](/handbook/values/#efficiency)
1. [Long Term Profitability Targets](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets)
1. [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis)
1. Open source with a lot of wider community contributors who make it easier to uncover customer demand for features and allow our organization to stay leaner.
1. A [single application](/handbook/product/single-application/) makes the user experience better, allows us to introduce new functionality to users, and it makes it easier for us to keep our velocity.
1. Run the same code for GitLab.com and and self-hosted applications and are [merging the CE and EE codebases](https://about.gitlab.com/2019/02/21/merging-ce-and-ee-codebases/)
1. How we [make decision](/handbook/leadership/#making-decisions)

### Serve smaller users

We have large competitors and smaller ones.
The larger competitors naturally get attention because we compete with them for large customers.
According to the innovators dilemmma](https://en.wikipedia.org/wiki/The_Innovator%27s_Dilemma): "the next generation product is not being built for the incumbent's customer set and this large customer set is not interested in the new innovation and keeps demanding more innovation with the incumbent product".
So it is really imporant that we also focus on the needs of smaller users since the next generation product will first be used by them.
If we don't do this we risk smaller competitors gaining marketshare there and then having the community and revenue to go up-market.

We serve smaller users by having:

1. A great free open source product that [gets the majority of new features](/company/stewardship/#promises).
1. A focus on [memory consumption reduction](/handbook/engineering/development/enablement/memory/) to ensure it is affordable to run our open source version.
1. A [tiered pricing model](/handbook/ceo/pricing/#four-tiers) with a very low pricepoint for our lowest tier.

## What isn't a risk

We're in a great market and have multiple waves that we're riding:

- [Digital transformation](/2019/03/19/reduce-cycle-time-digital-transformation/)
- [Cloud native and the adoption of Kubernetes](/cloud-native/)
- [Software eating the world](https://a16z.com/2011/08/20/why-software-is-eating-the-world/)
- [Customer Experience](/handbook/customer-success/vision/)
- [DevOps](/devops)
- [DevOps tooling consolidation](https://devops.com/challenges-devops-standardization/)
- [Microservices](/topics/microservices/)
- [Progressive delivery](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/)
- [Open source](/20-years-open-source/)
- [Workloads moving to the cloud](https://www.synopsys.com/blogs/software-security/cloud-migration-business/)
- [All remote](https://about.gitlab.com/company/culture/all-remote/)
