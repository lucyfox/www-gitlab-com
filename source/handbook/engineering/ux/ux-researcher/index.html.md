---
layout: markdown_page
title: "UX Researcher Workflow"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX Researcher onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX Research section of the handbook to help get you get oriented. There is also a specific page dedicated to [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding).

### How we decide what to research

UX Researchers collaborate with Product Managers to determine the scope and priority of research studies. UX Researchers should sit in on monthly planning calls for their relevant product stages to stay informed about what Product feels are the most important initiatives at GitLab. Additionally, UX Researchers should proactively offer ways in which they can assist in the delivery of such initiatives.

### UX Researcher tools

All UX Researchers have individual accounts for the following tools:

[SurveyMonkey](https://www.surveymonkey.com/) - All UX Researchers should utilize SurveyMonkey for surveys. Please use the `GitLab Theme` to style your survey. The GitLab logo should be positioned in the top left hand corner of every page (applied automatically, resize to `small`).

[UsabilityHub](https://usabilityhub.com/) - Used to build design evaluations, such as first click tests, preference tests and five second tests. UsabilityHub should not be used for surveys.

[MailChimp](https://mailchimp.com) - Used to send campaigns to subscribers of GitLab First Look.

[OptimalWorkshop](https://www.optimalworkshop.com) - Used for card sorts and tree testing. We do not have an ongoing subscription to OptimalWorkshop. We purchase a monthly license as and when required.

[Zoom Pro Account](https://zoom.us/) - We use Zoom to run usability testing sessions and user interviews.

### Research methods

We use a wide variety of research methods that include (but are not limited to):

* Usability testing (led by Product Design with guidance from UX Research)
* User interviews
* Surveys
* Beta testing
* Design evaluations (led by Product Design with guidance from UX Research) 
    * First click tests
    * Preference tests 
    * Five second tests
* Competitor analysis
* Card sorts
* Tree tests
* [Buy a feature](https://www.innovationgames.com/buy-a-feature/)
* Diary studies

#### Reviewing a design evaluation (guidance for UX Researchers)

1. UX Researchers should familiarize themselves with the process outlined in [How to create a design evaluation (guidance for Product Designers)](/handbook/engineering/ux/ux-designer/index.html#creating-a-design-evaluation-guidance-for-product-designers).

1. When sending a design evaluation to GitLab First Look users, ensure standard processes are followed; for example, distributing the study to a test sample of users and sending the study to GitLab First Look subscribers who have opted into design evaluations from the relevant product stage.

#### Incentives

* User interview or usability testing: $60 (or equivalent currency) Amazon Gift Card per 30 minutes.
* Surveys or card sorts: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift Cards.
* Beta testing: Unpaid.
* Design evaluations: Unpaid.

Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

### How we work

Like other departments at GitLab, UX Researchers follow the [GitLab Workflow](/handbook/communication/#everything-starts-with-an-issue) and the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline). 

We follow a monthly research cycle and use [milestones](https://docs.gitlab.com/ee/user/project/milestones/) to schedule our work. A typical monthly research cycle is as follows:

**8th of each month (or next business day)**
* Engineering/Product Kickoff
    
* UX Researchers begin working through their [UX Research checklist](/handbook/engineering/ux/ux-research/#checklist-templates).

**8th to 21st of each month.**
* UX Researchers undertake research.

**22nd of each month.**
* Release shipped.
    
* Product Managers and Product Designers start [Milestone Planning](/handbook/engineering/ux/ux-department-workflow/#milestone-planning) for the next release.

**22nd to the last day of the month.**
* UX Researchers analyze research and hold a wash-up meeting with relevant stakeholders.

**1st to 7th of each month.**
* UX Researchers document the study’s findings.
* UX Researchers begin preparing for research studies in the upcoming milestone (this might involve: gathering final requirements from stakeholders, drafting screeners, etc). 

Depending on the scope of each research study, a UX Researcher typically works on 1-3 studies per research cycle. Sometimes, larger studies may need to span more than 1 research cycle.

#### Scheduling issues

* UX Researchers assign themselves to issues. 

* When a new research proposal is raised in the [UX Research project](https://gitlab.com/gitlab-org/ux-research), you will be `@` mentioned to give your input. This doesn't mean that you have to start working on the issue right away. You should:

    * Quickly review the proposal, and assign the issue to yourself, if it relates to your product stage. 
    
    * Estimate the issue's priority by collaborating with the relevant Product Manager for the product stage.
    
    * Add a [milestone](https://docs.gitlab.com/ee/user/project/milestones/) to the issue. The milestone indicates when you plan to deliver the completed research to stakeholders. 
        
        For example: If you apply the milestone of `11.8` to an issue. Using the timescales outlined in [How we work](/handbook/engineering/ux/ux-research/#how-we-work). You will work on the research issue between 8th January to 7th February 2019. Ideally, you will discuss the results of the research with the relevant stakeholders between 22nd January to 31st January 2019 in order to assist with [Milestone Planning](/handbook/engineering/ux/ux-department-workflow/#milestone-planning).

* UX Researchers should aim to schedule 80% of their capacity to work on responsibilities outlined in the [role descriptions](/job-families/engineering/ux-researcher/) and try to block off the remaining 20% for other proactive work. This can be anything from exploring GitLab as a product to expanding your UX research skills by reading industry articles. 

#### Working on a research study

1. Update the `Research proposal` with the following:
    * Label the issue with the area of GitLab you’re testing (for example, `navigation`), the status of the issue (`in progress`) and the Product stage (for example, `manage`).
    * Add a milestone to the issue.
    * Mark the issue as `confidential` until the research is completed, so it doesn’t influence user behavior.
    * Assign the issue to yourself.
    * Add a checklist of actions that you plan to undertake. Next to each item, add an estimated deadline of when you plan to complete it. This makes it easier for people to understand where the research is up to.
    * Add related issue numbers.
1. Conduct the research. Ensure you keep the checklist up to date.
1. Document the study's findings within the [UXR_Insights repository](https://gitlab.com/gitlab-org/uxr_insights)
    * Refer to the repository's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) for instructions on how to do this.
    * Use the repository's issue templates to guide you through the process of adding your findings.
1. Schedule a wash-up meeting.
    * Ensure you link to the study's Epic in the calendar invitation at least 24 hours prior to the meeting.
1. Record and facilitate the wash-up meeting.
1. Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description.
1. Update the `Research proposal` with the following:
    * Link to the Epic.
    * Unmark the issue within the UX research project as `confidential`. (In some cases the issue may need to remain confidential if sensitive information is shared. If you’re unsure of whether an issue should remain confidential, please check with Sarah O’Donnell `@sarahod`).
    * Update the status of the issue to `done`.
    * Close the issue. You should stay assigned to closed issues so it's obvious who completed the research.

#### How to send a study to users of GitLab First Look.

Note: These instructions are for UX Researchers only. If you are not a UX Researcher but would like to send a study to users of GitLab First Look, please raise a [research proposal](/handbook/engineering/ux/ux-research//#how-to-request-research).

1. In MailChimp, create a new email campaign.
1. Click `Add recipients` under the `To` heading.
1. Choose `GitLab First Look` as your list.
1. Under `Segment or Tag` use the drop-down menu to select the segment of users you want to contact.
1. Leave the `From` field as is (emails are sent from `firstlook@gitlab.com`. Any emails sent to this address will forward to all UX Researchers).
1. Click `Add Subject`. Update the Subject field to: `Quick, new research study available!` and click `Save`.
1. Click `Design Email`. You are now going to design the content for your email campaign.
    * Click the `Saved templates` tab.
    * UX Research's templates are: `First Look - Survey`, `First Look - Usability Testing`, `First Look - User Interviews`, `First Look - Design Evaluations`, `First Look - Card Sort` and `First Look - Beta Testing`. Select the template you wish to use and click `Next`.
    * Review the content on the template, you will need to make some small alterations (such as researcher email address, dates of the study, etc). You can do this by clicking on the block of text you wish to change. This will bring up a WYSIWYG editor on the right-hand side of your screen.
    * You will also need to update the CTA URL to your study URL. You can do this by clicking on the CTA. This will bring up a WYSIWYG editor on the right-hand side of your screen where you can add a URL.
    * You should not make any styling changes to the templates. It's important that the design of emails from GitLab First Look stay consistent and are distinguishable from other email campaigns sent from GitLab.
1. Once you are ready to test your email campaign. Click `Preview and Test` in the top right corner of the screen.
    * `Enter preview mode` to see how your email will be displayed on Desktop and Mobile.
    * `Send a test email` to yourself. When the email arrives, double check the copy and any URLs.
1. Once you are happy with the design of your email campaign, click `Save & Close`.
1. You are now ready to send your email campaign! Click `Send` in the top right-hand corner of the screen.
1. Move the campaign to the `UX Research` folder.

#### Wash-up meetings
After a research initiative, the UX Researcher schedules a "Wash-up Meeting" with all stakeholders to help communicate findings and collaboratively agree on a list of improvements/actions to take forward.

* At least 24 hours prior to the wash-up meeting, the UX Researcher should add a Google document containing the research study’s key findings to the meeting’s calendar invitation.

* To ensure that the meeting is an effective use of everybody’s time, make sure to familiarize yourself with the study’s key findings prior to the meeting, and come prepared with suggestions.

### Checklist templates

The following are examples of checklists that you may want to add to a `Research proposal`.

#### Usability Testing
* Schedule users. (Deadline:)
* Write script. (Deadline:)
* Test the script. Conduct 1 usability testing session. Edit the script if required. (Deadline:)
* Conduct remaining usability testing sessions. (Deadline:)
* Pay users. (Deadline:)
* Analyze videos. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### User Interviews
* Schedule users. (Deadline:)
* Write interview guide. (Deadline:)
* Conduct the interviews. (Deadline:)
* Pay users. (Deadline:)
* Analyze videos. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Surveys
* Write survey questions. (Deadline:)
* Import survey questions into SurveyMonkey. (Deadline:)
* Test survey logic. (Deadline:)
* Distribute survey to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the survey. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyze survey results. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Design Evaluations
* Write instructions and/or questions.(Deadline:)
* Transfer instructions and/or questions into UsabilityHub. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyze responses. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Card Sorts
* Write questions.(Deadline:)
* Set-up the study in OptimalWorkshop.
* Transfer questions into OptimalWorkshop. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute study to remaining users. (Deadline:)
* Cleanse data and analyze responses. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)
